package jenko.b_comix;

import java.util.List;
import java.util.Map;

public class User{
    public String nickname;
    public String age;
    public String gendre;
    public String figure;
    public List<String> recent_chats;
    public Map<String,Object> friends;
    public Map<String,Object> all_books;

    public User(){

    }

    public User(String nickname,String age,String gendre,String figure,List<String> recents_chats,Map<String,Object> friends,Map<String,Object> all_books){
        this.nickname = nickname;
        this.age = age;
        this.gendre = gendre;
        this.figure = figure;
        this.recent_chats = recents_chats;
        this.friends = friends;
        this.all_books = all_books;
    }

}
