package jenko.b_comix;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;


public class MyAdapter extends android.support.v4.app.FragmentPagerAdapter {
    static final int NUM_ITEMS = 1;

    public MyAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public int getCount() {
        return NUM_ITEMS;
    }

    @Override
    public Fragment getItem(int position) {
        return RecentsFragment.newInstance();
    }
}
