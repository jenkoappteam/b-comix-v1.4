package jenko.b_comix;

import org.w3c.dom.Text;

public class ChatMessage {

    public String mFigure;
    public String mBackground;
    public String mText;
    public String mDstUserID;
    public String mSrcUserID;
    public String mBookName;

    public ChatMessage(String figure, String background,String text, String dstUserID, String srcUserID, String bookName){
        this.mFigure = figure;
        this.mBackground = background;
        this.mText = text;
        this.mDstUserID = dstUserID;
        this.mSrcUserID = srcUserID;
        this.mBookName = bookName;
    }

    public ChatMessage(){
    }


}
