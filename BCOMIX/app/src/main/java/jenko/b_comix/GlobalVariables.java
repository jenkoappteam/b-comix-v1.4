package jenko.b_comix;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class GlobalVariables {

    public static String mName;
    public static String mDstUserID;
    public static String mEmail;
    public static String mScreen;
    public static int mChlidCount;
    public static String mMyNickname;
    GlobalVariables(){}
    public void setName(String name){mName = name;}
    public void setEmail(String email){mEmail = email;}
    public void setScreen(String screen){mScreen = screen;}
    public void setChildCount(int count){mChlidCount = count;}
    public void setMyNickname(String name){mMyNickname = name;}
    public void setdstUserID(String id){mDstUserID = id;}

    public  String getName(){return mName ;}
    public  String getEmail(){return mEmail;}
    public  String getScreen(){return mScreen;}
    public  int getChildCount(){return mChlidCount;}
    public  String getMyNickname(){return mMyNickname;}
    public  String getDstUserID(){return mDstUserID ;}






}
