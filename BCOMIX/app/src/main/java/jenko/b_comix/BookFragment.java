package jenko.b_comix;

import android.content.Context;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.HashMap;
import java.util.Map;



public class BookFragment extends Fragment {
    private static final String ARG_PARAM1 = "mBackground_image_name";
    private static final String ARG_PARAM2 = "mBook_name";
    private static final String ARG_PARAM3 = "mCharecter1_image_name";
    private static final String ARG_PARAM4 = "mCharecter2_image_name";
    private static final String ARG_PARAM5 = "mIfUnread";

    private String mBackground_image_name;
    private String mBook_name;
    private String mCharecter1_image_name;
    private String mCharecter2_image_name;
    private Boolean mIfUnread;

    public BookFragment() {
        // Required empty public constructor
    }

    public static BookFragment newInstance(String background, String bookName, String charecter1,String charecter2,boolean ifUnread) {
        BookFragment fragment = new BookFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, background);
        args.putString(ARG_PARAM2, bookName);
        args.putString(ARG_PARAM3, charecter1);
        args.putString(ARG_PARAM4, charecter2);
        args.putString(ARG_PARAM5, String.valueOf(ifUnread));
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            this.mBackground_image_name = getArguments().getString(ARG_PARAM1);
            mBook_name = getArguments().getString(ARG_PARAM2);
            mCharecter1_image_name = getArguments().getString(ARG_PARAM3);
            mCharecter2_image_name = getArguments().getString(ARG_PARAM4);
            mIfUnread = getArguments().getBoolean(ARG_PARAM5);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_book, container, false);

        ImageView mBook_mockup_image_view = (ImageView) view.findViewById(R.id.bookImageView);
        TextView mBook_name_text_view = (TextView) view.findViewById(R.id.bookNameTextView);
        ImageView mCharecter_1_image_view = (ImageView) view.findViewById(R.id.Charecter1ImageView);
        ImageView mCharecter_2_image_view = (ImageView) view.findViewById(R.id.Charecter2ImageView);
        ImageView mUnread_sign_image_view = (ImageView) view.findViewById(R.id.newPages);

        if(this.mIfUnread){
            mUnread_sign_image_view.setVisibility(View.VISIBLE);
        }
        else
        {
            mUnread_sign_image_view.setVisibility(View.INVISIBLE);
        }

        Map<String,Object> background_map = new HashMap<>();
        background_map.put("dots_backs",R.drawable.dots_back_mockup);
        background_map.put("gray_back",R.drawable.gray_back_mockup);
        background_map.put("room_back",R.drawable.room_back_mockup);
        background_map.put("sea_back",R.drawable.sea_back_mockup);

        Map<String,Object> charecters_map = new HashMap<>();
        charecters_map.put("grandpa_with_guitar",R.drawable.grandpa_with_guitar);
        charecters_map.put("grandpa_on_chair",R.drawable.grandpa_on_chair);
        charecters_map.put("man_with_paresuit",R.drawable.man_with_paresuit);
        charecters_map.put("man_with_radio",R.drawable.man_with_radio);
        charecters_map.put("red_dress_women",R.drawable.red_dress_women);

        Typeface comicSensFont = Typeface.createFromAsset(getActivity().getAssets(), "comic_sens.ttf");
        mBook_name_text_view.setTypeface(comicSensFont);
        mBook_name_text_view.setText(this.mBook_name);

        mBook_mockup_image_view.setImageResource((int)background_map.get(this.mBackground_image_name));
        //mCharecter_1_image_view.setImageResource(Integer.parseInt(charecters_map.get(this.mCharecter1_image_name).toString()));
        //mCharecter_2_image_view.setImageResource(Integer.parseInt(charecters_map.get(this.mCharecter2_image_name).toString()));

        return view;
    }








}
