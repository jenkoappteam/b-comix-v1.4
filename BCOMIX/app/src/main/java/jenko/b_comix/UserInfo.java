package jenko.b_comix;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.util.ObjectsCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.ArraySet;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;

import org.w3c.dom.Text;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

import it.moondroid.coverflow.components.ui.containers.FeatureCoverFlow;


public class UserInfo extends AppCompatActivity {

    private static final String TAG = ".UserInfo";
    private ViewPager mSlideViewPager;
    private LinearLayout mDotLayout;
    private UserSlideAdapter sliderAdapter;
    private TextView[] mDots;
    boolean mCreateNamesSet;
    boolean mIfNameExiest;
    Map<String,Object> mExiestsnames;

    private int HasGendre = 0;
    private int curPos = 0;

    private String gendre;
    private String figure;
    private String mPhoneNumber;

    private FeatureCoverFlow coverFlow;
    private UserSlideAdapter adapter;
    private ArrayList<Character> Characters;

    private ImageView lightMale;
    private ImageView lightFemale;
    private ImageView male;
    private  ImageView female;
    private Button nextBtn;
    private TextView mNameExistsTextView;
    private DatabaseReference mDatabase;
    private DatabaseReference mExistsNicknamesDB;
    private FirebaseAuth mAuth;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_info);
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mExistsNicknamesDB = FirebaseDatabase.getInstance().getReference().child("nicknames");

        coverFlow = (FeatureCoverFlow) findViewById(R.id.coverflow);

        settingDummyData();
        adapter = new UserSlideAdapter(this, Characters);
        coverFlow.setAdapter(adapter);
        coverFlow.setOnScrollPositionListener(onScrollListener());


        male = (ImageView) findViewById(R.id.MaleImageView);
        female = (ImageView) findViewById(R.id.FemaleImageView);
        lightMale = (ImageView) findViewById(R.id.MaleLightImageView);
        lightFemale = (ImageView) findViewById(R.id.FemaleLightImageView);
        nextBtn = (Button) findViewById(R.id.next_btn);


        Typeface comicSensFont = Typeface.createFromAsset(getAssets(), "comic_sens.ttf");
        final EditText name_edit_text = (EditText) findViewById(R.id.nickNameEditText);
        final EditText age_edit_text = (EditText) findViewById(R.id.AgeEditText);
        name_edit_text.setTypeface(comicSensFont);
        age_edit_text.setTypeface(comicSensFont);

        mNameExistsTextView = (TextView)findViewById(R.id.nameExistsTextView);
        mExiestsnames = new HashMap<>();
        mIfNameExiest = false;
        name_edit_text.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                mExistsNicknamesDB.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if(dataSnapshot.exists()){
                            if(dataSnapshot.getValue()==null){
                                mCreateNamesSet = true;
                            }
                            else{
                                mExiestsnames = (Map<String, Object>) dataSnapshot.getValue();
                            }
                        }
                    }
                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }
        });


        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (HasGendre > 0 && (name_edit_text.getText() == null || age_edit_text.getText() == null)) {
                    Toast.makeText(UserInfo.this, "All fields must be filled.", Toast.LENGTH_SHORT).show();
                } else {
                    if (true) {
                        Map<String,Object> friends = new HashMap<>();
                        Map<String,Object> all_books = new HashMap<>();
                        List<String> recentChats = new ArrayList<>();


                        if(mCreateNamesSet)
                        {
                            GlobalVariables global = new GlobalVariables();
                            global.setMyNickname(name_edit_text.getText().toString());
                            name_edit_text.setEnabled(false);
                            Map<String,Object> usersNameMap = new HashMap<>();
                            usersNameMap.put(mAuth.getUid().toString(),name_edit_text.getText().toString());
                            mExistsNicknamesDB.setValue(usersNameMap);
                            User new_user = new User(name_edit_text.getText().toString(),age_edit_text.getText().toString(),gendre,figure,recentChats,friends,all_books);
                            writeNewUser(mAuth.getUid(),new_user);
                            Intent serviceIntent = new Intent(UserInfo.this,ServiceFriendsRequest.class);
                            startService(serviceIntent);
                            startActivity(new Intent(UserInfo.this, LaunchActivity.class));
                            finish();

                        }
                        else
                        {
                            GlobalVariables global = new GlobalVariables();
                            global.setMyNickname(name_edit_text.getText().toString());
                            //mExiestsnames.put(mAuth.getUid().toString(),name_edit_text.getText().toString());
                            mExistsNicknamesDB.setValue(mExiestsnames);
                            User new_user = new User(name_edit_text.getText().toString(),age_edit_text.getText().toString(),gendre,figure,recentChats,friends,all_books);
                            writeNewUser(mAuth.getUid(),new_user);
                            Intent serviceIntent = new Intent(UserInfo.this,ServiceFriendsRequest.class);
                            startService(serviceIntent);
                            startActivity(new Intent(UserInfo.this, LaunchActivity.class));
                            finish();

                        }

                    }else{
                        Toast.makeText(UserInfo.this, "Email Already Exist", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });


        lightMale.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onClick(View v) {
                HasGendre++;
                gendre = "male";
                if (male.getVisibility() == View.INVISIBLE) {
                    male.setVisibility(View.VISIBLE);
                    female.setVisibility(View.INVISIBLE);
                } else if (male.getVisibility() == View.VISIBLE) {
                    male.setVisibility(View.INVISIBLE);
                }
                name_edit_text.setTextColor(getResources().getColor(R.color.colorNotBlue));
                age_edit_text.setTextColor(getResources().getColor(R.color.colorNotBlue));
                name_edit_text.setHintTextColor(getResources().getColor(R.color.colorNotBlue));
                age_edit_text.setHintTextColor(getResources().getColor(R.color.colorNotBlue));

            }
        });

        lightFemale.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onClick(View v) {
                gendre = "female";
                HasGendre++;
                if (female.getVisibility() == View.INVISIBLE) {
                    female.setVisibility(View.VISIBLE);
                    male.setVisibility(View.INVISIBLE);
                } else if (female.getVisibility() == View.VISIBLE) {
                    female.setVisibility(View.INVISIBLE);
                }
                name_edit_text.setTextColor(getResources().getColor(R.color.colorPink));
                age_edit_text.setTextColor(getResources().getColor(R.color.colorPink));
                name_edit_text.setHintTextColor(getResources().getColor(R.color.colorPink));
                age_edit_text.setHintTextColor(getResources().getColor(R.color.colorPink));
            }
        });



    }

    private FeatureCoverFlow.OnScrollPositionListener onScrollListener() {
        return new FeatureCoverFlow.OnScrollPositionListener() {
            @Override
            public void onScrolledToPosition(int position) {
                Log.v("MainActiivty", "position: " + position);
                pickFigure(position);
            }

            @Override
            public void onScrolling() {
                Log.i("MainActivity", "scrolling");
            }
        };
    }


    private void writeNewUser(String userId,User user){
        DatabaseReference usernameDB = FirebaseDatabase.getInstance().getReference().child("nicknames");
        mExiestsnames.put(mAuth.getUid(),user.nickname);
        usernameDB.setValue(mExiestsnames);
        mDatabase.child("users").child(userId).setValue(user);
    }


    private void settingDummyData() {
        Characters = new ArrayList<>();
        Characters.add(new Character(R.drawable.grandpa_on_chair));
        Characters.add(new Character(R.drawable.grandpa_with_guitar));
        Characters.add(new Character(R.drawable.man_with_paresuit));
        Characters.add(new Character(R.drawable.man_with_radio));
        Characters.add(new Character(R.drawable.red_dress_women));
    }


    private void pickFigure(int curPos) {
        switch(curPos){
            case 0:
                figure = "grandpa_with_guitar";
                break;
            case 1:
                figure = "grandpa_on_chair";
                break;
            case 2:
                figure = "man_with_radio";
                break;
            case 3:
                figure = "man_with_paresuit";
                break;
            case 4:
                figure = "red_dress_women";
                break;
            default:
                figure = "red_dress_women";
                break;
        }
    }


    ViewPager.OnPageChangeListener viewListener = new ViewPager.OnPageChangeListener(){

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {


        }

        @Override
        public void onPageSelected(int position) {

        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };







}