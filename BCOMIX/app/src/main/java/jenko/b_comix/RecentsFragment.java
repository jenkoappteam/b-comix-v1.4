package jenko.b_comix;

import android.graphics.drawable.Drawable;
import android.media.Image;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.DynamicLayout;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;
import com.google.firebase.auth.FirebaseAuth;

import java.util.Random;
import java.util.zip.Inflater;

public class RecentsFragment extends Fragment {
    View ContainerContent;
    boolean Appearance;

    FirebaseAuth mAuth;
    ViewGroup temp1;

    static RecentsFragment newInstance() {
        RecentsFragment f = new RecentsFragment();
        return f;
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.materialrecents_activity_recents, container, false);
        temp1 = container;
        ContainerContent = getLayoutInflater().inflate(R.layout.activity_recents, container, false);
        //final int[] colors = new int[]{0xff7fffff, 0xffff7fff, 0xffffff7f, 0xff7f7fff, 0xffff7f7f, 0xff7fff7f}; - example for arrays
        GlobalVariables.mChlidCount++;
        RecentsList recents = (RecentsList) v.findViewById(R.id.recents);
        recents.setAdapter(new RecentsAdapter() {
            @Override
            public String getTitle(int position) {
                mAuth = FirebaseAuth.getInstance();
                return mAuth.getCurrentUser().getEmail();
            }

            @Override
            public View getView(int position) {
                ImageView iv = new ImageView(getActivity());
                iv.setScaleType(ImageView.ScaleType.CENTER_CROP);
                iv.setImageResource(R.drawable.room_back);
                iv.setBackgroundColor(0xffffffff);
                //View temp = inflater.inflate(R.layout.activity_chat_struct,temp1,false);
                //ImageView bg = (ImageView)temp.findViewById(R.id.BackGroundViewImage);
                //bg.setImageResource(R.drawable.sea_backs_quare);
                //ImageView ch = (ImageView)temp.findViewById(R.id.CharecterImageView);
                //ch.setImageResource(R.drawable.man_with_paresuit);
                return iv;
            }

            @Override
            public Drawable getIcon(int position) {
                return getResources().getDrawable(R.mipmap.ic_launcher);
            }

            @Override
            public int getHeaderColor(int position) {
                return R.color.bodyTextColor;
            }

            @Override
            public int getCount() {
                return GlobalVariables.mChlidCount;
            }
        });

        recents.setOnItemClickListener(new RecentsList.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int i) {
                onItemExpand(Appearance,view);
                //Toast.makeText(view.getContext(), "Card " + i + " clicked", Toast.LENGTH_SHORT).show();
            }
        });
        return v;
    }

    public void onItemExpand(boolean b,View view) {
        //mContainerContent.setVisibility(b ? View.VISIBLE: View.GONE);
        if (b) {
            ViewAppearByEffect(ContainerContent);
            Toast.makeText(view.getContext(), "Card " + " clicked", Toast.LENGTH_SHORT).show();
            Appearance = !Appearance;
        } else {
            ContainerContent.setVisibility(View.GONE);
            Appearance = !Appearance;
        }
    }

    public void ViewAppearByEffect(View view)
    {
        view.setAlpha(0);
        view.setVisibility(View.VISIBLE);
        view.animate().alpha(1.0f).setDuration(300);
        //view.animate().rotation(360);
    }

}
