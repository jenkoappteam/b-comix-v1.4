package jenko.b_comix;

import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.ArraySet;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import java.util.stream.Collectors;

public class RecentsChats extends AppCompatActivity {

    LinearLayout mBook_layout;
    List<String> mRecentChats;
    Map<String,Object> mAll_books_data;
    FirebaseFirestore db;
    FirebaseAuth mAuth;
    DocumentReference mUserDoc;

    @SuppressWarnings("unchecked")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recents_chats);
        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();

        //this.mRecentChats = new String[0];

        this.mBook_layout = (LinearLayout)findViewById(R.id.BooksLinearlayout);

        this.mUserDoc = FirebaseFirestore.getInstance().document("users/"+ mAuth.getCurrentUser().getEmail());

        this.mUserDoc.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {

            }
        });

        this.mUserDoc.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                if(documentSnapshot.exists()){
                    Map<String,Object> doc_map = documentSnapshot.getData();
                    mRecentChats = (ArrayList<String>) documentSnapshot.get("recent_chats");
                    mAll_books_data = (HashMap<String,Object>)documentSnapshot.get("all_books");


                    if(mRecentChats.size() ==0){
                        //TODO: print a msg
                    }
                    else{
                        for(String p : mRecentChats){
                            String [] parts = p.split("###");
                            String friend_nickname = parts[0];
                            String book_number = parts[1];

                            String background;
                            String book_name;
                            String creation_date;
                            String friend_figure;
                            String my_figure;

                            //Map<String,Object> all_books = (HashMap<String, Object>)mAll_books_data.get("all_books");
                            Map<String,Object> friend_book = (HashMap<String, Object>)mAll_books_data.get(friend_nickname);
                            Map<String,Object> book = (HashMap<String, Object>)friend_book.get(book_number);
                            Map<String,Object> book_details = (HashMap<String, Object>)book.get("details");

                            background = (String) book_details.get("book_background");
                            book_name = (String) book_details.get("book_name");
                            creation_date = (String) book_details.get("creation_date");
                            friend_figure = (String) book_details.get("friend_figure");
                            my_figure = (String) book_details.get("my_figure");

                            BookFragment fragment = BookFragment.newInstance(background,book_name,my_figure,friend_figure,false);
                            getSupportFragmentManager()
                                    .beginTransaction()
                                    .add(R.id.BooksLinearlayout,fragment,"myTag")
                                    .commit();



                        }
                    }
                }
            }
        });











    }
}
