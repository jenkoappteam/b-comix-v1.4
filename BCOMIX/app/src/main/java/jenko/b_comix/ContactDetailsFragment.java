package jenko.b_comix;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;


public class ContactDetailsFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_PARAM3 = "param3";


    // TODO: Rename and change types of parameters
    private String mContactName;
    private String mContactPhone;
    private String mContactImage;
    FirebaseAuth mAuth;
    GlobalVariables global;


    public ContactDetailsFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static ContactDetailsFragment newInstance(String contact_name, String contact_phone,String contact_image) {
        ContactDetailsFragment fragment = new ContactDetailsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, contact_name);
        args.putString(ARG_PARAM2, contact_phone);
        args.putString(ARG_PARAM3, contact_image);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mContactName = getArguments().getString(ARG_PARAM1);
            mContactPhone = getArguments().getString(ARG_PARAM2);
            mContactImage = getArguments().getString(ARG_PARAM3);
        }
    }

    @SuppressLint("SetTextI18n")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_contact_details, container, false);
        ImageView contactImageView = (ImageView)view.findViewById(R.id.ContactImageView);
        TextView contactNameTextView = (TextView)view.findViewById(R.id.ContactNameTextView);
        TextView contactPhoneTextView = (TextView)view.findViewById(R.id.ContactPhoneTextView);
        mAuth = FirebaseAuth.getInstance();
        Map<String,Object> images_map = new HashMap<>();
        images_map.put("red",R.drawable.contact_sign_red);
        images_map.put("green",R.drawable.contact_sign_green);
        images_map.put("blue",R.drawable.contact_sign_blue);
        images_map.put("yellow",R.drawable.contact_sign_red);
        //contactImageView.setImageResource((Integer) images_map.get(this.mContactImage));

        switch (mContactImage){
            case "red":
                contactImageView.setImageResource(R.drawable.contact_sign_red);
                break;
            case "green":
                contactImageView.setImageResource(R.drawable.contact_sign_green);
                break;
            case "blue":
                contactImageView.setImageResource(R.drawable.contact_sign_blue);
                break;
            case "yellow":
                contactImageView.setImageResource(R.drawable.contact_sign_yellow);
                break;
        }

        ConstraintLayout layoyut = (ConstraintLayout)view.findViewById(R.id.ContactFragmentLayout);


        if(mContactPhone.startsWith("search")){
            contactNameTextView.setText("Contact Name: "+this.mContactName);
            contactPhoneTextView.setText("");
            layoyut.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which) {
                                case DialogInterface.BUTTON_POSITIVE:
                                    String [] parts = mContactPhone.split("&&");
                                    DatabaseReference database = FirebaseDatabase.getInstance().getReference();
                                    global = new GlobalVariables();
                                    sendRequest(parts[1],database,mAuth.getUid()+"&&"+global.getMyNickname());

                                    break;

                                case DialogInterface.BUTTON_NEGATIVE:
                                    //No button clicked
                                    break;
                            }
                        }
                    };
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity()).setMessage("Send request to " + mContactName + "?").setPositiveButton("Yes", dialogClickListener).setNegativeButton("No", dialogClickListener);
                    builder.show();
                }
            });
        }
        else {
            contactNameTextView.setText("Contact Name: "+this.mContactName);
            contactPhoneTextView.setText("Contact Phone: "+this.mContactPhone);
            layoyut.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which) {
                                case DialogInterface.BUTTON_POSITIVE:
                                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("sms:" + mContactPhone));
                                    intent.putExtra("sms_body", "Dont be gay and download B-COMIX app!");
                                    startActivity(intent);
                                    break;

                                case DialogInterface.BUTTON_NEGATIVE:
                                    //No button clicked
                                    break;
                            }
                        }
                    };
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity()).setMessage("Send Invite to " + mContactName + "?").setPositiveButton("Yes", dialogClickListener).setNegativeButton("No", dialogClickListener);
                    builder.show();
                }

            });
        }

        return view;
    }

    public void sendRequest( String friendId,DatabaseReference friendDB, String myId){
        DatabaseReference newRequestDB = friendDB.child("friends_request").child(friendId).push();
        newRequestDB.setValue("request&&"+myId);
        getActivity().onBackPressed();
    }



}
