package jenko.b_comix;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class ReciverMessagesService extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        context.startService(new Intent(context,ServiceMessagesListener.class));
    }
}
